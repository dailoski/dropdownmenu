import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ExpandMore from '@material-ui/icons/ExpandMore';



const styles = theme => ({
  root: {
    display: 'inline-flex',
    flexWrap: 'wrap',
    width: 'fit-content'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit ,
  },
  select:{
    borderRadius: '7px'
    },
  selectMenu:{
    
    
    borderRadius: "2px"
  },
  menuContainer: {
    // translate(moveXAxis, moveYAxis) ako treba samp Y onda moze i translateY(value), oba primaju i negativne vrednosti
    transform: "translate(0px,60px)",
  },
  menu:{},
  label:{
    color:"red !important",
  }
});
class Dropdown extends React.Component {
  state = {
    selected: '',
  };

  
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.root} autoComplete="off">
        <FormControl variant="filled" className={classes.formControl}>
          <InputLabel 
          className={classes.label}
          classes={{
            focused: classes.focused
          }}
          >
          Select2
          </InputLabel>
          <Select
            IconComponent={ExpandMore}
            className={classes.select}
            disableUnderline
            value={this.state.selected}
            onChange={this.handleChange}
            input={<FilledInput name="selected"  />}
            MenuProps={{className:classes.menuContainer,
              MenuListProps:{
                className:classes.menu
              }
            }}
            
          >
            
            <MenuItem value={1}>First</MenuItem>
            <MenuItem value={2}>Second</MenuItem>
            <MenuItem value={3}>Third</MenuItem>
          </Select>
        </FormControl>
      </form>
    );
  }
}

Dropdown.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Dropdown);
