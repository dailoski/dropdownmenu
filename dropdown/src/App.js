import React, { Component } from 'react';
import './App.css';
import Dropdown from './Dropdown';
import { CssBaseline } from '@material-ui/core';

class App extends Component {
  render() {
    return (
      <div className="App">
      <CssBaseline/>
       <Dropdown />
      </div>
    );
  }
}

export default App;
